# s6-linux-init - tools to create an s6-based Linux init system
-------------------------------------------------------------

 s6-linux-init is a suite of tools designed to provide an init program
 for the Linux kernel - i.e., the first program run at boot time.

 See https://skarnet.org/software/s6-linux-init/ for details.


## Installation
  ------------

 See the INSTALL file.

## Recommended build instructions

The recommended method to build this package directly from git.
```
gbp clone https://gitlab.com/init-diversity/s6-rc/s6-linux-init.git && 
cd s6-linux-init.git && 
gbp buildpackage -uc -us
```

The following should get you all the software required to build using this method:

```
sudo apt install git-buildpackage skalibs-dev libexecline-dev libs6-dev s6
```

## Customization

You can customize paths via flags given to configure. See `./configure --help` for a list of all available configure options.

These flags will need to be added to the `debian/rules` file.



* Contact information
  -------------------

 Laurent Bercot <ska-skaware at skarnet.org>

 Please use the <skaware at list.skarnet.org> mailing-list for
questions about the inner workings of s6, and the
<supervision at list.skarnet.org> mailing-list for questions
about process supervision, init systems, and so on.
